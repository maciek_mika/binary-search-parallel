package com.Mika;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class Producer {

    private static String url = "failover:(tcp://169.254.1.1:61616,localhost:8161)";
    private static String subject = "testQueue1"; // Queue Name

    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        Connection connection = connectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destination = session.createQueue(subject);
        MessageProducer producer = session.createProducer(destination);

        String str = "12 15 21 100 121 01 11 19 54";

        TextMessage message = session.createTextMessage(str);
        producer.send(message);
        System.out.println("Sent Message '" + message.getText() + "'");
        connection.close();
    }
}
