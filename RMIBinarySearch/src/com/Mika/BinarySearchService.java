package com.Mika;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface BinarySearchService extends Remote {
    void ping() throws RemoteException;
    String sendMessage(String message) throws RemoteException;
    int executeBinarySearch(int[] arr, int l, int r, int x) throws RemoteException;

    void setTask(Task task) throws RemoteException;
    Task getTask() throws RemoteException;
}
