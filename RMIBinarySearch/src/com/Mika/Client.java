package com.Mika;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.time.LocalDateTime;

public class Client {

    public static void main(String[] args) throws RemoteException, UnknownHostException {
        Client client = new Client();
        String serviceHost = Server.DefaultMasterNodeName;
        // connect to the remote service host
        if (args.length > 0)
            serviceHost = args[0];

        BinarySearchService service = Server.connect(serviceHost);

        client.run(service);
    }

    void run(BinarySearchService service) throws UnknownHostException, RemoteException {
        String localHostname = InetAddress.getLocalHost().getHostName();
        System.out.println("This is host: " + localHostname );

        int arrTestSets[] = {100,1000,10000,25000,50000,100000,250000,500000,1000000,2500000};
        int arrXTestSets[] = {58,302,6890,14433,22000,94321,75000,13131,432665,1894321};

        long t1, t2;

        t1 = System.currentTimeMillis();
        service.ping();
        t2 = System.currentTimeMillis();
        System.out.println("Ping took " + (t2-t1) + " ms.");

        t1 = System.nanoTime();
        String greeting = service.sendMessage("Hello World at " + LocalDateTime.now());
        t2 = System.nanoTime();

        System.out.println("Client side:" + greeting);
        System.out.println("SendMessage took " + (t2-t1) + " ms.");
        for (int m = 0; m < 10; m++) {

            int arrTestSet[] = generateArray(arrTestSets[m]);
            int nTestSet = arrTestSet.length;
            int xTestSet = arrXTestSets[m];
            t1 = System.nanoTime();
            int test = service.executeBinarySearch(arrTestSet, 0, nTestSet -1, xTestSet);
            if (test != -1) {
                t2 = System.nanoTime();
                System.out.println("Binary search took: " + (t2-t1) + " | element was found at " + test);
            }

            initNodes(service, 2, arrTestSet, xTestSet);

            initNodes(service, 3, arrTestSet, xTestSet);
        }
    }

    int[] generateArray(int size){
        int[] array = new int[size];
        for (int i = 1; i <= size; i++){
            array[i-1] = i;
        }
        return array;
    }

    void initNodes(BinarySearchService service, int amountOfNodes, int[] array, int x) throws RemoteException {
        Thread threads[] = new Thread[amountOfNodes];
        int divideElement = array.length / amountOfNodes;
        int startElement = 0;
        int partArray = divideElement;
        int k = 2;
        long t1 = System.nanoTime();

        for (int i = 0; i < amountOfNodes; i++){
            int finalStartElement = startElement;
            int finalPartArray = partArray;
            threads[i] = new Thread() {
                public void run(){
                    int test = 0;
                    try {
                        test = service.executeBinarySearch(array, finalStartElement, finalPartArray - 1, x);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    if (test != -1) {
                        long t2 = System.nanoTime();
                        System.out.println("Binary search nodes "+ amountOfNodes +": " + (t2-t1) + " | element was found at " + test);
                    }
                }
            };

            startElement = partArray;
            partArray = divideElement*(k);
            k++;
        }

        for (int s = 0; s < threads.length; s++){
            threads[s].start();
        }

        for (int g = 0; g < threads.length; g++){
            try{
                threads[g].join();
            } catch (Exception e){
                System.out.println(e);
            }
        }
    }
}
