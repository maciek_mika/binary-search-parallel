package com.Mika;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server extends UnicastRemoteObject implements BinarySearchService {

    /**
     * To run the server on your local machine, you need to add multiple "main" methods.
     * the one in this class (Server class) and the one in the Client class
     * !!!Also change the DefaultMasterNodeName to your machines localhost name!!!
     */
    static final String DefaultMasterNodeName = "MacBook-Pro-van-Maciek.local";
    private static final String ServiceName = "TaskServer";
    private static final int DefaultPort = 6969;

    private static String localHostname;

    Task task;

    Server() throws RemoteException {

    }

    public static BinarySearchService connect(String host) {
        BinarySearchService remoteService = null;

        try {
            System.out.println("Connecting to "+ host);

            Registry r = LocateRegistry.getRegistry(host, DefaultPort);
            remoteService = (BinarySearchService) r.lookup(ServiceName);

        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

        return remoteService;
    }

    public void ping() throws RemoteException {
        return;
    }

    public String sendMessage(String message) throws RemoteException {
        System.out.println("echo: " + message);
        return message + " received at " + localHostname;
    }

    public int executeBinarySearch(int[] arr, int l, int r, int x) throws RemoteException {
        BinarySearch bo = new BinarySearch();
        return bo.binarySearch(arr, l, r, x);
    }

    public void setTask(Task task) throws RemoteException {
        this.task = task;
    }

    public Task getTask() throws RemoteException {
        return this.task;
    }

    public static void main(String[] args) {
        try {
            // get the hostname of this node
            localHostname = InetAddress.getLocalHost().getHostName();

            // start a new server object
            Server server = new Server();

            // start the registry service on this node
            Registry registry = LocateRegistry.createRegistry(Server.DefaultPort);

            // add binding to this server object and use a specific ServiceName to reference it
            registry.bind(Server.ServiceName, server);


            System.out.println(Server.ServiceName + " running on " + localHostname);

        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public static class BinarySearch {

        public int binarySearch(int arr[], int l, int r, int x) {
            if (r >= l) {
                int mid = l + (r - l) / 2;

                // If the element is present at the
                // middle itself
                if (arr[mid] == x)
                    return mid;

                // If element is smaller than mid, then
                // it can only be present in left subarray
                if (arr[mid] > x)
                    return binarySearch(arr, l, mid - 1, x);

                // Else the element can only be present
                // in right subarray
                return binarySearch(arr, mid + 1, r, x);
            }

            // We reach here when element is not present
            // in array
            return -1;
        }
    }
}
