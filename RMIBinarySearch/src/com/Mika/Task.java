package com.Mika;

public interface Task<T> {
    int binarySearch(int[] arr, int l, int r, int x);
}
