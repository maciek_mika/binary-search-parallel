package com.Mika;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

public class BinarySearchBlockingConsumer implements Runnable {
    private ArrayBlockingQueue queue;
    private int x;
    private long startTime;
    private int numberTestSet;
    private int[] arr;
    private int amountOfThreads;

    public void run(){
        int[] array = new int[0];
        try {
            array = (int[]) queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            int l = array[0];
            int r = array[1];
            int resultTestSet = binarySearch(arr, l, (r-1), x);
            if (resultTestSet != -1){
                long endTime = System.nanoTime();
                long totalTime = endTime - startTime;
                System.out.println("BLOCKING - TEST SET "+numberTestSet+" THREADS "+amountOfThreads+": Element found at index " + resultTestSet + " total time: "+totalTime);
            }
    }

    public BinarySearchBlockingConsumer(int[] arr, int x, long startTime, int numberTestSet, ArrayBlockingQueue queue, int amountOfThreads){
        this.arr = arr;
        this.x = x;
        this.startTime = startTime;
        this.numberTestSet = numberTestSet;
        this.queue = queue;
        this.amountOfThreads = amountOfThreads;
    }

    int binarySearch(int arr[], int l, int r, int x) {
        if (r >= l) {
            int mid = l + (r - l) / 2;

            // If the element is present at the
            // middle itself
            if (arr[mid] == x)
                return mid;

            // If element is smaller than mid, then
            // it can only be present in left subarray
            if (arr[mid] > x)
                return binarySearch(arr, l, mid - 1, x);

            // Else the element can only be present
            // in right subarray
            return binarySearch(arr, mid + 1, r, x);
        }

        // We reach here when element is not present
        // in array
        return -1;
    }
}
