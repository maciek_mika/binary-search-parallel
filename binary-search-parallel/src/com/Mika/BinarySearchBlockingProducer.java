package com.Mika;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

public class BinarySearchBlockingProducer implements Runnable {
    private int[] arr;
    private int amountOfThreads;
    protected ArrayBlockingQueue queue;

    public BinarySearchBlockingProducer(int[] arr, ArrayBlockingQueue queue, int amountOfThreads){
        this.arr = arr;
        this.amountOfThreads= amountOfThreads;
        this.queue = queue;
    }

    public void run(){
        int divideElement = arr.length / amountOfThreads;
        int partArray = divideElement;
        int startElement = 0;
        int finalStartElement = startElement;
        int k = 2;

        //send left and right
        for (int i = 0; i < amountOfThreads; i++){
            int[] array = {startElement, partArray};
            startElement = partArray;
            partArray = divideElement*(k);
            k++;
            try {
                queue.put(array);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
