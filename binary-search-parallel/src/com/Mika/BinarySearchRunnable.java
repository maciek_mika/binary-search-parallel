package com.Mika;

public class BinarySearchRunnable implements Runnable{
    private int arr[];
    private int l;
    private int r;
    private int x;
    private long startTime;
    private int amountOfThreads;
    private int numberTestSet;

    public BinarySearchRunnable(int arr[], int l, int r, int x, long startTime, int amountOfThreads, int numberTestSet){
        this.arr = arr;
        this.l = l;
        this.r = r;
        this.x = x;
        this.startTime = startTime;
        this.amountOfThreads= amountOfThreads;
        this.numberTestSet = numberTestSet;
    }

    public void run(){
        int resultTestSet = binarySearch(arr, l, r ,x);
        if (resultTestSet != -1){
            long endTime = System.nanoTime();
            long totalTime = endTime - startTime;
            System.out.println("THREADPOOL - TEST SET "+numberTestSet+" THREADS "+amountOfThreads+": Element found at index " + resultTestSet + " total time: "+totalTime);
        }
    }

    int binarySearch(int arr[], int l, int r, int x) {
        if (r >= l) {
            int mid = l + (r - l) / 2;

            // If the element is present at the
            // middle itself
            if (arr[mid] == x)
                return mid;

            // If element is smaller than mid, then
            // it can only be present in left subarray
            if (arr[mid] > x)
                return binarySearch(arr, l, mid - 1, x);

            // Else the element can only be present
            // in right subarray
            return binarySearch(arr, mid + 1, r, x);
        }

        // We reach here when element is not present
        // in array
        return -1;
    }
}
