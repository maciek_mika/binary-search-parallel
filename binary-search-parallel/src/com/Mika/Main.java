package com.Mika;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Main main = new Main();

        main.run();
    }

    public void run() throws InterruptedException {
        BinarySearch ob = new BinarySearch();

        /*
           Testcases
         */
        //test case 1
        int arrTestSets[] = {100,1000,10000,25000,50000,100000,250000,500000,1000000,2500000};
        int arrXTestSets[] = {58,302,6890,14433,22000,94321,75000,13131,432665,1894321};
        //10,100,1000,2500,5000,10000,25000,50000,100000,250000

        for (int m = 0; m < 10; m++){
            int arrTestSet[] = generateArray(arrTestSets[m]);
            AtomicIntegerArray arrTestSetAtomic = generateAtomicArray(arrTestSets[m]);
            ConcurrentHashMap arrTestSetHashMap = generateHashMap(arrTestSets[m]);
            int nTestSet = arrTestSet.length;
            int xTestSet = arrXTestSets[m];

            long startTimeSerial = System.nanoTime();
            int resultTestSetSerial = ob.binarySearch(arrTestSet, 0, nTestSet - 1, xTestSet);
            long endTimeSerial = System.nanoTime();
            long totalTimeSerial = endTimeSerial - startTimeSerial;
            if (resultTestSetSerial == -1) {
                System.out.println("Element not present");
            } else {
                System.out.println("SERIAL - TEST SET "+(m+1)+": Element found at index " + resultTestSetSerial + ", total time: " + totalTimeSerial);
            }

            //Threads and locks version 1
            ParallelBinarySearch(2, arrTestSet, xTestSet, (m+1));
            ParallelBinarySearch(4, arrTestSet, xTestSet, (m+1));
            ParallelBinarySearch(8, arrTestSet, xTestSet, (m+1));

            //ThreadPool without any concurrent data structures
            initExecutorThreads(2, arrTestSet, xTestSet, (m+1));
            initExecutorThreads(4, arrTestSet, xTestSet, (m+1));
            initExecutorThreads(8, arrTestSet, xTestSet, (m+1));

            //ThreadPool AtomicIntegerArray
            initAtomicThreads(2, arrTestSetAtomic, xTestSet, (m+1));
            initAtomicThreads(4, arrTestSetAtomic, xTestSet, (m+1));
            initAtomicThreads(8, arrTestSetAtomic, xTestSet, (m+1));

            //ThreadPool ConcurrentHashMap
            initConcurrentHashMap(2, arrTestSetHashMap, xTestSet, (m+1));
            initConcurrentHashMap(4, arrTestSetHashMap, xTestSet, (m+1));
            initConcurrentHashMap(8, arrTestSetHashMap, xTestSet, (m+1));

            //ThreadPool ArrayBlockingQueue
            initArrayBlockingQueue(2, arrTestSet, xTestSet, (m+1));
            initArrayBlockingQueue(4, arrTestSet, xTestSet, (m+1));
            initArrayBlockingQueue(8, arrTestSet, xTestSet, (m+1));
        }
    }

    public void ParallelBinarySearch(int amountOfThreads, int[] array, int x, int numberTestSet) {
        Thread threads[] = new Thread[amountOfThreads];
        BinarySearch ob = new BinarySearch();
        int divideElement = array.length / amountOfThreads;
        int startElement = 0;
        int partArray = divideElement;
        int k = 2;
        long startTime = System.nanoTime();

        for (int i = 0; i < amountOfThreads; i++){
            int finalStartElement = startElement;
            int finalPartArray = partArray;
            threads[i] = new Thread() {
                public void run(){
                    int resultTestSet = ob.binarySearch(array, finalStartElement, finalPartArray -1, x);
                    if (resultTestSet != -1){
                        long endTime = System.nanoTime();
                        long totalTime = endTime - startTime;
                        System.out.println("PARALLEL - TEST SET "+numberTestSet+" THREADS "+amountOfThreads+": Element found at index " + resultTestSet + " total time: "+totalTime);
                    }
                }
            };

            startElement = partArray;
            partArray = divideElement*(k);
            k++;
        }

        for (int s = 0; s < threads.length; s++){
            threads[s].start();
        }

        for (int g = 0; g < threads.length; g++){
            try{
                threads[g].join();
            } catch (Exception e){
                System.out.println(e);
            }
        }
    }

    public int[] generateArray(int size){
        int[] array = new int[size];
        for (int i = 1; i <= size; i++){
            array[i-1] = i;
        }
        return array;
    }

    AtomicIntegerArray generateAtomicArray(int size){
        AtomicIntegerArray array = new AtomicIntegerArray(size);
        for (int i = 1; i <= size; i++){
            array.set((i-1), i);
        }
        return array;
    }

    ConcurrentHashMap generateHashMap(int size){
        ConcurrentHashMap array = new ConcurrentHashMap(size);
        for (int i = 1; i <= size; i++){
            array.put((i-1),(i-1));
        }
        return array;
    }

    void initExecutorThreads(int amountOfThreads, int[] array, int x, int numberTestSet){
        ExecutorService executorService = Executors.newFixedThreadPool(amountOfThreads);
        int divideElement = array.length / amountOfThreads;
        int startElement = 0;
        int partArray = divideElement;
        int k = 2;
        long startTime = System.nanoTime();

        for (int i = 0; i < amountOfThreads; i++){
            int finalStartElement = startElement;
            int finalPartArray = partArray;
            BinarySearchRunnable binarySearchRunnable = new BinarySearchRunnable(array, finalStartElement, finalPartArray -1, x, startTime, amountOfThreads, numberTestSet);
            executorService.execute(binarySearchRunnable);

            startElement = partArray;
            partArray = divideElement*(k);
            k++;
        }

        executorService.shutdown();
    }

    void initAtomicThreads(int amountOfThreads, AtomicIntegerArray array, int x, int numberTestSet){
        ExecutorService executorService = Executors.newFixedThreadPool(amountOfThreads);
        int divideElement = array.length() / amountOfThreads;
        int startElement = 0;
        int partArray = divideElement;
        int k = 2;
        long startTime = System.nanoTime();

        for (int i = 0; i < amountOfThreads; i++){
            int finalStartElement = startElement;
            int finalPartArray = partArray;
            BinarySearchAtomicIntegerArray binarySearchRunnable = new BinarySearchAtomicIntegerArray(array, finalStartElement, finalPartArray -1, x, startTime, amountOfThreads, numberTestSet);
            executorService.execute(binarySearchRunnable);

            startElement = partArray;
            partArray = divideElement*(k);
            k++;
        }

        executorService.shutdown();
    }

    void initConcurrentHashMap(int amountOfThreads, ConcurrentHashMap array, int x, int numberTestSet){
        ExecutorService executorService = Executors.newFixedThreadPool(amountOfThreads);
        int divideElement = array.size() / amountOfThreads;
        int startElement = 0;
        int partArray = divideElement;
        int k = 2;
        long startTime = System.nanoTime();

        for (int i = 0; i < amountOfThreads; i++){
            int finalStartElement = startElement;
            int finalPartArray = partArray;
            BinarySearchConcurrentHashMap binarySearchRunnable = new BinarySearchConcurrentHashMap(array, finalStartElement, finalPartArray -1, x, startTime, amountOfThreads, numberTestSet);
            executorService.execute(binarySearchRunnable);

            startElement = partArray;
            partArray = divideElement*(k);
            k++;
        }

        executorService.shutdown();
    }

    void initArrayBlockingQueue(int amountOfThreads, int[] arr, int x, int numberTestSet){
        ExecutorService executorService = Executors.newFixedThreadPool(amountOfThreads);
        long startTime = System.nanoTime();
        ArrayBlockingQueue queue = new ArrayBlockingQueue(arr.length -1);
        BinarySearchBlockingProducer producer = new BinarySearchBlockingProducer(arr, queue, amountOfThreads);

        executorService.execute(producer);

        for (int i = 0; i < amountOfThreads; i++){
            int newArr[] = arr;
            BinarySearchBlockingConsumer consumer = new BinarySearchBlockingConsumer(newArr, x, startTime, numberTestSet, queue, amountOfThreads);
            executorService.execute(consumer);
        }

        executorService.shutdown();
    }
}
