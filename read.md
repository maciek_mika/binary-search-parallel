#Parallel computing binary search algorithm

##authors

- Maciek Mika (500709568)
- Farah Jama (500789387)


##Usage

use `git clone git@bitbucket.org:maciek_mika/binary-search-parallel.git` to download the repository

Here you will find 2 projects

- parallel-binary-search : this project contains the parallel implementations of binary search algorithm

- RMIBinarySearch : this project contains the RMI implementation of the binary search algorithm


##Running RMIBinarySearch

to run this project you need to assign 2 `main` run methodes. one in the `Server` class and one in the `Client` class.

Run the `Server` class and check what the `terminal` says after `TaskServer running on`. that is your `DefaultMasterNodeName`. change that in your `Server` class.

after that run the `main` method in the `Client` class.